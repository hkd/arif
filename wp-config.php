<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'bd_auto' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ly><PNtt#czU!E.Rf6}_6wsn#W1bCeT-`1=>pvRf?O`xoIO~Bq/ojRQwoubnyg}D' );
define( 'SECURE_AUTH_KEY',  'M/KU~9S}/RLWr,Pf^XcD8]8;~/z6P~ 8UKm@K ouh!%$a `%TNF(`&Z`TVW!=}}*' );
define( 'LOGGED_IN_KEY',    ': MVGS>ms,dgN_x>8J&t,nfMnA4_F?^q3}R.$G<K6n$DQ+nR13U`4Ik2n&Mcds/W' );
define( 'NONCE_KEY',        '-.UD9BuL1;^%y:Bl ik69_%0G-[5.SWa$~i_D=(7Fk(ni/x(nxA__*q$~8og;x<#' );
define( 'AUTH_SALT',        'z$ZEa%5zwrI[WekLv{eAGhh V^!tVHIiT}h/,.bn~+IPbOm6pE*(uzb[l.XY{j7{' );
define( 'SECURE_AUTH_SALT', '{?U+uY].+(wV|JlkfH>,=Z~|&*b7af/$Zhn%90(M_I58fJuN{i%g!Q9*}0b{TQKE' );
define( 'LOGGED_IN_SALT',   'PV-`)JZx[q_*N}8|iKWZ]O<0it1CgUQcjDVoCoG$SN,HdA2&[n0SB:FYv)A)Of17' );
define( 'NONCE_SALT',       'B)htP(6~>C^w2Tbv=:J|`W-2w+s{}wt!hbvh1p1v[rs^BOAbp>)>IO!5CR_))pJG' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
